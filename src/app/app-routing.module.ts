import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { ListeComponent } from "./liste/liste.component";
import { FilmComponent } from "./film/film.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'liste', component: ListeComponent },
  { path: 'film/:id', component: FilmComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
