import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Film } from './film';



const urlApi = 'https://api.themoviedb.org/3/search/movie?api_key=c3af6e7e4c27a28e85087793724dbc43&language=fr&query=';
@Injectable({
  providedIn: 'root'
})
export class FilmService {

  

  private infos: any=[];

  constructor(private http: HttpClient) { }
  public sendGetRequest(film: string){ 
      return this.http.get(urlApi + film +'&page=1&include_adult=false');
        //catchError(this.handleError)
    }

    GetAllInfos(){
      return this.infos
    }

    addInfos(Film: Film){
      this.infos = Film;
    }
   /* private handleError(error: HttpErrorResponse): any {
      if (error.error instanceof ErrorEvent) {
        console.error('An error occurred:', error.error.message);
      } else {
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
      }
      return throwError(
        'Something bad happened; please try again later.'
        );
    }*/
  }

