import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FilmService } from '../film.service';
import { NgModule } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @NgModule({
    providers: [
      FilmService,
    ]
  })

  identForm!: FormGroup;
  isSubmitted = false;
  TabFilm: any = [];

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      film: new FormControl(),
    });
  }
  film() {
    this.isSubmitted = true;
    console.log('Données du formulaire...', this.identForm.value);

    this.filmService.sendGetRequest(this.identForm.value.film).subscribe((data: any) => {

      //this.filmService.addInfos(data['results']);
      // console.log(JSON.parse(data));
      this.filmService.addInfos(data['results']);
      this.TabFilm = this.filmService.GetAllInfos();
      //let keys = Object.keys(data);
      console.log(data['results']);

    })
  }

}
