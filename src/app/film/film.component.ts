import { Component, OnInit } from '@angular/core';
import { FilmService } from '../film.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']

})


export class FilmComponent implements OnInit {

    constructor(private filmService: FilmService) {
      console.log(this.filmService);
    }

  ngOnInit(): void {
    console.log('Le composant a fini son initialisation');

  }
}