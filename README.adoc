== Introduction

L’exercice de programmation que l’on nous a porposée pour objectif de  permettre de consolider les premiers concepts présentés dans les premiers TD d’introduction à Angular.

== Prérequis :

Avoir étudié/réalisé les travaux dirigés de 1.1 à 2.3 (sur vinsio)

  ● Environnement de travail opérationnel
  ● Création d’un projet et de ses composants.
  ● Avoir Angular d'installer

== Objectif :

On souhaite offrir à des utilisateurs une application web leur permettant de gérer une liste de leurs films préférés.
L'utilisateur pourra evaluer les films qu'il aura retenus (commentaire + évaluation de 1 à 5)

== Attendu techniques :

1. Interrogation d'une API existante via la saisie d'information dans un formulaire
2. Définition D'au moins une entité objet pour la représentation des données
3. Affichage de la liste des objets, accès au détail, avec sauvegarde locale côté client
4. Test unitaire (4 au minimmum)

== Contraintes techniques

1. L'application sera devoloppée en TS avec angular et s'appuiera sur l'API TMDB
2. La liste des films retenus par l'utilisateur sera sauvegarder (LocalStorage)
3. Limiter le nombre de requête sur l'API

== Analyse du code

Pour présenter ce code, nous allons suivre le scénario typique d'utilisation. Dans un premier temps on crée le formulaire qui permette de demander a l'utilisteur de rentrer le film dont il souhaite affiché le poster le titre et le synopsys puis la boucle *ngFor="let leFilm of TabFilm"> qui nous permet d'afficher le tableau du film demandé
[, html]
----
<section class="section">
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-half">
                <h3 class="title is-3">Recherche de film</h3>
                <form [formGroup]="identForm" (ngSubmit)="film()">
                    <div class="field"> 
                        <label class="label">Filmothèque</label>
                        <div class="control"> 
                            <input class="input is-success" formControlName="film" type="text" placeholder="Entrez un film" value="">
                        </div>
                    </div>
                    <div class="field is-grouped">
                        <div class="control"> 
                            <button class="button is-link">Soumettre</button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<ul *ngFor="let leFilm of TabFilm">
    <div>
        <a [routerLink]="['/film/', leFilm.id]">
            <li>
                Titre : {{ leFilm.title }}
                Synopsis : {{ leFilm.overview }}
            </li>
                <li><img src="https://image.tmdb.org/t/p/w500{{leFilm.poster_path}}" width="150"></li>
        </a>
    </div>
</ul>
----

== patie 2
test unitaire

dans cette partie 

[, TS]
----
constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      film: new FormControl(),
    });
  }
  film() {
    this.isSubmitted = true;
    console.log('Données du formulaire...', this.identForm.value);

    this.filmService.sendGetRequest(this.identForm.value.film).subscribe((data: any) => {

      //this.filmService.addInfos(data['results']);
      // console.log(JSON.parse(data));
      this.filmService.addInfos(data['results']);
      this.TabFilm = this.filmService.GetAllInfos();
      //let keys = Object.keys(data);
      console.log(data['results']);

    })
  }

}
----
envoie d'une requete avec information recupere dans le formulaire le parametre any car on ne connait pas la valeur retournée dans le tableau precedement on envoie les infos demandées dans le test unitaire

== Conclusion

Pour conclure, notre projet permet d'afficher la liste de film demander lorsque l'uitilisateur rentre le nom d'un film

Liens vers mon dépôt Gitlab :https://gitlab.com/estebansalmon/filmotec




